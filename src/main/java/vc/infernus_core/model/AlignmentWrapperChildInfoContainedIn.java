package vc.infernus_core.model;

public class AlignmentWrapperChildInfoContainedIn extends AlignmentWrapperChildInfo {

	@Override
	public String format() {
		return "Supp. alignment is inside the range of primary alignment";
	}

}
