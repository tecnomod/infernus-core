package vc.infernus_core.model;

public class AlignmentWrapperChildInfoTraslocation extends AlignmentWrapperChildInfo {
	
	private String chrParent;
	private String chrChild;
	
	public AlignmentWrapperChildInfoTraslocation(String chrParent, String chrChild) {
		super();
		this.chrParent = chrParent;
		this.chrChild = chrChild;
	}
	
	@Override
	public String format() {
		return "Possible translocation from primary alignment in " +
				chrParent + " to " + chrChild;
	}




	public String getChrParent() {
		return chrParent;
	}




	public void setChrParent(String chrParent) {
		this.chrParent = chrParent;
	}




	public String getChrChild() {
		return chrChild;
	}




	public void setChrChild(String chrChild) {
		this.chrChild = chrChild;
	}

}
