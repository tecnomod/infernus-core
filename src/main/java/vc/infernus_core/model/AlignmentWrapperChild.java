package vc.infernus_core.model;

import java.util.LinkedList;
import java.util.List;

import htsjdk.samtools.SAMRecord;

public class AlignmentWrapperChild extends AlignmentWrapper {

	private static final long serialVersionUID = -2955019158840740129L;
	private List<AlignmentWrapperChildInfo> infoList;

	public AlignmentWrapperChild(String chromosome, SAMRecord alignment) {
		super(chromosome, alignment);
		infoList = new LinkedList<AlignmentWrapperChildInfo>();
	}
	
	public void calculateInformation(AlignmentWrapper parent) {
		long parentStart = parent.getAlignment().getStart();
		long parentEnd = parent.getAlignment().getEnd();
		long childStart = getAlignment().getStart();
		long childEnd = getAlignment().getEnd();
		
		if (parent.getChromosome().equals(this.getChromosome())) {
			long distance = 0;
			if (childStart > parentEnd) { // Read on the right
				distance = childStart - parentEnd;
			} else if (childEnd < parentStart) { // Read on the left
				distance = childEnd - parentStart;
			}
			if (distance == 0) {
				infoList.add(new AlignmentWrapperChildInfoContainedIn());
			} else {
				infoList.add(new AlignmentWrapperChildInfoDistance(distance));
			}
		} else {
			infoList.add(new AlignmentWrapperChildInfoTraslocation(parent.getChromosome(), getChromosome()));
		}
	}
	
	
	
	@Override
	public String toString() {
		SAMRecord alignment = this.getAlignment();
		StringBuilder sb = new StringBuilder();
		sb.append("\t- ");
		sb.append(this.getChromosome()).append(":");
		sb.append(alignment.getStart()).append("-");
		sb.append(alignment.getEnd()).append("\t");
		sb.append("length: " + alignment.getReadLength()).append("\t");
		sb.append("\n");
		for (AlignmentWrapperChildInfo info: infoList) {
			sb.append("\t| " + info.format());
		}
		
		return sb.toString();
	}

	public List<AlignmentWrapperChildInfo> getInfoList() {
		return infoList;
	}

	public void setInfoList(List<AlignmentWrapperChildInfo> infoList) {
		this.infoList = infoList;
	}

}
