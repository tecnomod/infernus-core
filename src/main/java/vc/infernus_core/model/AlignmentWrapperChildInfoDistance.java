package vc.infernus_core.model;

public class AlignmentWrapperChildInfoDistance extends AlignmentWrapperChildInfo {
	private Long distance;
	
	public AlignmentWrapperChildInfoDistance(Long distance) {
		this.distance = distance;
	}

	public Long getDistance() {
		return distance;
	}

	public void setDistance(Long distance) {
		this.distance = distance;
	}

	@Override
	public String format() {
		return "Distance: " + distance + " bp";
	}
}
