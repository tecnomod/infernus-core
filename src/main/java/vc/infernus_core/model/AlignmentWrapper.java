package vc.infernus_core.model;

import java.io.Serializable;

import htsjdk.samtools.SAMRecord;

public class AlignmentWrapper implements Serializable {
	
	private static final long serialVersionUID = -6598172806020933331L;

	private String chromosome;
	private SAMRecord alignment;
	public AlignmentWrapper(String chromosome, SAMRecord alignment) {
		super();
		this.chromosome = chromosome;
		this.alignment = alignment;
	}
	public String getChromosome() {
		return chromosome;
	}
	public void setChromosome(String chromosome) {
		this.chromosome = chromosome;
	}
	public SAMRecord getAlignment() {
		return alignment;
	}
	public void setAlignment(SAMRecord alignment) {
		this.alignment = alignment;
	}
	@Override
	public String toString() {
		SAMRecord alignment = this.getAlignment();
		StringBuilder sb = new StringBuilder();
		sb.append("> " + alignment.getReadName()).append("\t");
		sb.append(this.getChromosome()).append(":");
		sb.append(alignment.getStart()).append("-");
		sb.append(alignment.getEnd()).append("\t");
		sb.append("length: " + alignment.getReadLength());
		return sb.toString();
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((alignment == null) ? 0 : alignment.hashCode());
		result = prime * result + ((chromosome == null) ? 0 : chromosome.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AlignmentWrapper other = (AlignmentWrapper) obj;
		if (alignment == null) {
			if (other.alignment != null)
				return false;
		} else if (!alignment.equals(other.alignment))
			return false;
		if (chromosome == null) {
			if (other.chromosome != null)
				return false;
		} else if (!chromosome.equals(other.chromosome))
			return false;
		return true;
	}
	
	
}
