package vc.infernus_core.main;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import htsjdk.samtools.SamReader;
import vc.infernus_core.factory.FilterFactory;
import vc.infernus_core.controller.ArgumentController;
import vc.infernus_core.interfaces.BAMParser;
import vc.infernus_core.interfaces.IFilter;
import vc.infernus_core.interfaces.IFilterFactory;
import vc.infernus_core.model.AlignmentWrapper;
import vc.infernus_core.model.AlignmentWrapperChild;
import vc.infernus_core.parser.BAMParserImpl;
import vc.infernus_core.utils.ArgumentUtils;
import vc.infernus_core.utils.BAMUtils;

public class App  {
	public static final String APP_NAME = "infernus";
	public static final String APP_VERSION = "1.1.0";
	
	private static Logger logger = LoggerFactory.getLogger(App.class);
	private static BAMUtils bamUtils = new BAMUtils();
	
    public static void main( String[] args ) throws IOException {
    	ArgumentController argumentController = ArgumentController.getInstance();
    	argumentController.parse(args);
    	String inputFile = argumentController.getOptionValue(ArgumentUtils.INPUT_FILE_SORT);
    	
    	List<IFilter> filters = getFilters(argumentController);
    	BAMParser bamParser = new BAMParserImpl();
    	
    	SamReader reader = bamParser.parse(inputFile);
		Map<AlignmentWrapper, List<AlignmentWrapperChild>> parentChildren = bamUtils.getSupplementaryReadsTree(filters, reader);
		reader.close();
		bamUtils.printMap(parentChildren);
    }
    
    private static List<IFilter> getFilters(ArgumentController argumentController) {
    	List<IFilter> filterList = new LinkedList<IFilter>();
    	IFilterFactory filterFactory = new FilterFactory();
    	
    	for (String filter : ArgumentUtils.FILTER_OPTIONS) {
    		if (argumentController.existsFlag(filter)) {
        		filterList.add(filterFactory.getFilter(filter));
        	}
    	}
    	return filterList;
    }
}
