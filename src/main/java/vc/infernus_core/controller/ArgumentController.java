package vc.infernus_core.controller;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vc.infernus_core.main.App;
import vc.infernus_core.utils.ArgumentUtils;

public class ArgumentController {
	private static Logger logger = LoggerFactory.getLogger(ArgumentController.class);

	private Options options;
	private CommandLine cmd;
	private static ArgumentController instance = null;

	private ArgumentController() {
		options = new Options();
		setUpOptions();
	}

	public void setUpOptions() {
		Option help = new Option(ArgumentUtils.HELP_SORT,
				ArgumentUtils.HELP_LONG, false, ArgumentUtils.HELP_DESC);
		help.setRequired(false);
		
		Option input = new Option(ArgumentUtils.INPUT_FILE_SORT,
				ArgumentUtils.INPUT_FILE_LONG, true, ArgumentUtils.INPUT_FILE_DESC);
		input.setRequired(true);
		
		Option filterNoContained = new Option(null,
				ArgumentUtils.F_NOCONTAINED_LONG, false, ArgumentUtils.F_NOCONTAINED_DESC);
		
		Option filterNoDistance = new Option(null,
				ArgumentUtils.F_NODISTANCE_LONG, false, ArgumentUtils.F_NODISTANCE_DESC);
		
		Option filterNoTraslocation = new Option(null,
				ArgumentUtils.F_NOTRASLOC_LONG, false, ArgumentUtils.F_NOTRASLOC_DESC);
		
		
		options.addOption(help)
			.addOption(input)
			.addOption(filterNoContained)
			.addOption(filterNoDistance)
			.addOption(filterNoTraslocation);
	}
	
	public static ArgumentController getInstance() {
		if (instance == null) {
			instance = new ArgumentController();
		}
		return instance;
	}

	public void parse(String[] args) {
		CommandLineParser parser = new DefaultParser();
		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.out.println(e.getMessage());
			helpAndExit();
		}
		if (cmd.hasOption(ArgumentUtils.HELP_SORT) || 
				cmd.hasOption(ArgumentUtils.HELP_LONG)) {
			helpAndExit();
		}
	}
	
	public boolean existsFlag(String flag) {
		return cmd.hasOption(flag);
	}

	public String getOptionValue(String option) {
		if (cmd == null) {
			String msg = "cmd is null, didn't you call parse()?";
			logger.debug(msg);
			throw new NullPointerException(msg);
		}
		return cmd.getOptionValue(option);
	}
	
	public Option getOption(String option) {
		return options.getOption(option);
	}
	
	public void printArguments() {
		if (options == null) {
			logger.debug("I did not load any option");
		} else {
			logger.debug("Loaded arguments are:");
			for (Option option : options.getOptions()) {
				String shortOpt = ((option.getOpt() != null)? option.getOpt():"None");
				String longOpt = ((option.getLongOpt() != null)? option.getLongOpt():"None");
				String value = getOptionValue(option.getLongOpt());
				logger.debug("Short: " + shortOpt + " Long: " + longOpt + " Value: " + ((value != null)? ": " + value:"None"));
			}
		}
	}
	
	private void helpAndExit() {
		HelpFormatter formatter = new HelpFormatter();
		StringBuilder usage = new StringBuilder();
		usage.append("-" + ArgumentUtils.INPUT_FILE_SORT + " file.bam ");
		formatter.printHelp(App.APP_NAME + " " + usage, options);
		System.exit(1);
	}
}
