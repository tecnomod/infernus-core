package vc.infernus_core.parser;

import java.io.File;
import java.io.FileNotFoundException;

import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;
import vc.infernus_core.interfaces.BAMParser;

public class BAMParserImpl implements BAMParser{

	public SamReader parse(String filePath) throws FileNotFoundException {
		return parse(new File(filePath));
	}

	public SamReader parse(File file) throws FileNotFoundException {
		if(file == null || !file.exists()){
			throw new FileNotFoundException(file.getAbsolutePath());
		}
		
		//SamReader samReader = SamReaderFactory.makeDefault().open(file);
		SamReader samReader= SamReaderFactory.
		        makeDefault().
		        validationStringency(ValidationStringency.SILENT).
		        open(file);
		return samReader;
	}

}
