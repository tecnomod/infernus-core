package vc.infernus_core.interfaces;

public interface IFilterFactory {
	public IFilter getFilter(String filter);
}
