package vc.infernus_core.interfaces;

import vc.infernus_core.model.AlignmentWrapperChild;

public interface IFilter {
	boolean apply(AlignmentWrapperChild alignment);
}
