package vc.infernus_core.interfaces;

import java.io.File;
import java.io.FileNotFoundException;

import htsjdk.samtools.SamReader;

public interface BAMParser {
	SamReader parse(String filePath)throws FileNotFoundException;
	SamReader parse(File file) throws FileNotFoundException;
}
