package vc.infernus_core.filter;

import vc.infernus_core.interfaces.IFilter;
import vc.infernus_core.model.AlignmentWrapperChild;
import vc.infernus_core.model.AlignmentWrapperChildInfo;
import vc.infernus_core.model.AlignmentWrapperChildInfoDistance;

public class FilterNoDistance implements IFilter {
		
	@Override
	public boolean apply(AlignmentWrapperChild alignment) {
		for (AlignmentWrapperChildInfo childInfo : alignment.getInfoList()) {
			if (childInfo instanceof AlignmentWrapperChildInfoDistance) {
				return false;
			}
		}
		return true;
	}
}
