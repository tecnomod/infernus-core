package vc.infernus_core.factory;

import vc.infernus_core.filter.FilterNoDistance;
import vc.infernus_core.filter.FilterNoContained;
import vc.infernus_core.filter.FilterNoTraslocation;
import vc.infernus_core.interfaces.IFilter;
import vc.infernus_core.interfaces.IFilterFactory;
import vc.infernus_core.utils.ArgumentUtils;

public class FilterFactory implements IFilterFactory {

	@Override
	public IFilter getFilter(String filter) {
		if (filter != null) {
			switch (filter) {
			case ArgumentUtils.F_NOCONTAINED_LONG:
				return new FilterNoContained();
			case ArgumentUtils.F_NODISTANCE_LONG:
				return new FilterNoDistance();
			case ArgumentUtils.F_NOTRASLOC_LONG:
				return new FilterNoTraslocation();
			default:
				return null;
			}
		}
		return null;
	}

}
