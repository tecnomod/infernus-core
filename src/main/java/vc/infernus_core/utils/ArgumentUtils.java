package vc.infernus_core.utils;

import java.util.Arrays;
import java.util.List;

public class ArgumentUtils {
	public static final String HELP_SORT = "h";
	public static final String HELP_LONG = "help";
	public static final String HELP_DESC = "this message";
	public static final String INPUT_FILE_SORT = "i";
	public static final String INPUT_FILE_LONG = "input";
	public static final String INPUT_FILE_DESC = "Input BAM file";
	
	public static final String F_NOCONTAINED_LONG = "nocontained";
	public static final String F_NOCONTAINED_DESC = "Filters out " + 
			"supplementary alignment contained in the range of its primary alignment";
	public static final String F_NODISTANCE_LONG = "nodistance";
	public static final String F_NODISTANCE_DESC = "Filters out " + 
			"supplementary alignment whose distance is not 0 bp";
	public static final String F_NOTRASLOC_LONG = "notrasloc";
	public static final String F_NOTRASLOC_DESC = "Filters out " + 
			"supplementary alignment placed in a different chromosome from its primary alignment";
	
	public static final List<String> FILTER_OPTIONS = 
			Arrays.asList(F_NOCONTAINED_LONG, F_NODISTANCE_LONG, F_NOTRASLOC_LONG);
}
