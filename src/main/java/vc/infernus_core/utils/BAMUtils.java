package vc.infernus_core.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SAMRecordIterator;
import htsjdk.samtools.SAMSequenceRecord;
import htsjdk.samtools.SamReader;
import vc.infernus_core.interfaces.IFilter;
import vc.infernus_core.model.AlignmentWrapper;
import vc.infernus_core.model.AlignmentWrapperChild;

public class BAMUtils {
	
	public Map<AlignmentWrapper, List<AlignmentWrapperChild>> 
		getSupplementaryReadsTree(List<IFilter> filters, SamReader reader) throws IOException{
		
		List<AlignmentWrapperChild> childAlingments = new ArrayList<AlignmentWrapperChild>();
		List<AlignmentWrapper> primaryAlingments = new ArrayList<AlignmentWrapper>();
		Map<AlignmentWrapper, List<AlignmentWrapperChild>> parentChildren = new HashMap<>();
		
		List<SAMSequenceRecord> chrList= reader.getFileHeader().getSequenceDictionary().getSequences();
		for(SAMSequenceRecord chr : chrList){
		    SAMRecordIterator iterator = reader.query(chr.getSequenceName(), 0, 0, false);
			while (iterator.hasNext()) {
				SAMRecord samRecord = iterator.next();
				if (samRecord.getSupplementaryAlignmentFlag()) {
					childAlingments.add(new AlignmentWrapperChild(chr.getSequenceName(), samRecord));
				} else if(!samRecord.isSecondaryOrSupplementary()) {
					primaryAlingments.add(new AlignmentWrapper(chr.getSequenceName(), samRecord));
				}
			}
			iterator.close();
		}
		
		for (AlignmentWrapperChild child : childAlingments) {
			for(AlignmentWrapper parent : primaryAlingments){
				if(parent.getAlignment().getReadName().equals(child.getAlignment().getReadName())){
					child.calculateInformation(parent);
					boolean applied = filterApplied(filters, child);
					if (applied) {
						this.addChild(parent, child, parentChildren);
					}
				}
			}
		}
		
		return parentChildren;
	}

	private boolean filterApplied(List<IFilter> filters, AlignmentWrapperChild child) {
		boolean applied = true;
		if (filters != null) {
			for (IFilter filter: filters) {
				if (!filter.apply(child)) {
					applied = false;
					break;
				}
			}
		}
		return applied;
	}
	
	private void addChild(AlignmentWrapper parent, AlignmentWrapperChild child, Map<AlignmentWrapper, List<AlignmentWrapperChild>> parentChildren){
		if(!parentChildren.containsKey(parent)){
			parentChildren.put(parent, new ArrayList<AlignmentWrapperChild>());
		}
		parentChildren.get(parent).add(child);
	}
	
	
	public void printMap(Map<AlignmentWrapper, List<AlignmentWrapperChild>> parentChildren){
		if(parentChildren != null){
			for(Entry<AlignmentWrapper, List<AlignmentWrapperChild>> entry : parentChildren.entrySet()){
				AlignmentWrapper parent = entry.getKey();
				List<AlignmentWrapperChild> children = entry.getValue();
				System.out.println(parent.toString());
				for(AlignmentWrapper child : children){
					System.out.println(child.toString());
				}
//				System.out.println("-------------------------------------------------------------");
			}
		}
	}
}
