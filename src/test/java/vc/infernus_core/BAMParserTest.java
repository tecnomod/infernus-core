package vc.infernus_core;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.Test;

import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SamReader;
import vc.infernus_core.interfaces.BAMParser;
import vc.infernus_core.model.AlignmentWrapper;
import vc.infernus_core.model.AlignmentWrapperChild;
import vc.infernus_core.parser.BAMParserImpl;
import vc.infernus_core.utils.BAMUtils;

public class BAMParserTest {
	private final static String BAM_TEST_FILE = "src/test/resources/chr1.filtered.bam";
	private BAMParser bamParser = new BAMParserImpl();
	private BAMUtils bamUtils = new BAMUtils();

	@Test
	public void testParseString() throws IOException {
		SamReader reader = bamParser.parse(BAM_TEST_FILE);
//		Map<SAMRecord, List<SAMRecord>> parentChildren = bamUtils.getSupplementaryReadsTree(reader);
		Map<AlignmentWrapper, List<AlignmentWrapperChild>> parentChildren = bamUtils.getSupplementaryReadsTree(null, reader);
		reader.close();
//		this.printMap(parentChildren);
		this.printMap(parentChildren);
	}
	

	
	private void printMap2(Map<SAMRecord, List<SAMRecord>> parentChildren){
		if(parentChildren != null){
			for(Entry<SAMRecord, List<SAMRecord>> entry : parentChildren.entrySet()){
				SAMRecord parent = entry.getKey();
				List<SAMRecord> children = entry.getValue();
				System.out.println(printSAMRecord(parent));
				for(SAMRecord child : children){
					System.out.println("\t"+printSAMRecord(child));
				}
				System.out.println("-------------------------------------------------------------");
			}
		}
	}
	
	private void printMap(Map<AlignmentWrapper, List<AlignmentWrapperChild>> parentChildren){
		if(parentChildren != null){
			for(Entry<AlignmentWrapper, List<AlignmentWrapperChild>> entry : parentChildren.entrySet()){
				AlignmentWrapper parent = entry.getKey();
				List<AlignmentWrapperChild> children = entry.getValue();
				System.out.println(parent.toString());
				for(AlignmentWrapper child : children){
					System.out.println(child.toString());
				}
				System.out.println("-------------------------------------------------------------");
			}
		}
	}
	
	private String printSAMRecord(SAMRecord alignment){
		StringBuilder sb = new StringBuilder();
		sb.append(alignment.getReadName()).append("\t");
		sb.append(alignment.getStart()).append("-");
		sb.append(alignment.getEnd()).append("\t");
		sb.append(alignment.getReadLength());
		return sb.toString();
	}

}
